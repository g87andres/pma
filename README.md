## About

Simple shell script that automatically installs the latest version of PhpMyAdmin
on a Laravel Homestead box.

## Usage

1. SSH into your Homestead box and `cd` to your code/projects directory

2. `$ curl -sS https://bitbucket.org/g87andres/pma/raw/4336a1966cce4599d86777d87134f1377f2c5cad/pma.sh | sh`

3. Open the `/etc/hosts` file on your main machine and add
```127.0.0.1  phpmyadmin.dev```

4. Go to [http://phpmyadmin.dev:8000](http://phpmyadmin.dev:8000)